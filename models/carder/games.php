<?php
namespace Carder;
class Games extends \Blank\Database {
	public $id;
	public $title;
	public $user_id;

	public function listByUserId($userId) {
		return $this->select('*')->where('user_id', $userId)->run();
	}

	public function getById($gameId) {
		return $this->select('*')->where('id', $gameId)->run('object');
	}

	public function getByUserGameId($userId, $gameId) {
		return $this->select('*')->where(array('id' => $gameId, 'user_id' => $userId))->run('object');
	}

	public function getByTitle($title) {
		return $this->select('*')->where('title %', $userId)->run();
	}

	public function addGame($data) {
		$dataList = $this->filterData($data);
		$dataList['created'] = time();
		$dataList['tstamp']  = time();
		return $this->insert($dataList)->run();
	}

	public function saveGame($data) {
		$dataList = $this->filterData($data);
		$dataList['tstamp'] = time();
		$id = $dataList['id'];
		unset($dataList['id']);
		return $this->update($dataList)->where('id', $id)->showQuery()->run();
	}

	public function delGame($gameId) {
		return $this->delete('id', $gameId)->run();
	}

	private function filterData($data) {
		$list = array('id', 'title', 'user_id', 'created', 'tstamp', 'gamedata');
		$dataList = $data->toArrayRecursive();
		foreach ($dataList as $key => $value) {
			if (!in_array($key, $list))
				unset($dataList[$key]);
		}
		return $dataList;
	}
}