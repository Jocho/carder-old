<?php
namespace Carder;
class Decks extends \Blank\Database {
	public $id;
	public $name;
	public $game_id;

	public function getById($gameId) {
		return $this->select('*')->where('id', $gameId)->run('object');
	}

	public function getByGameId($gameId) {
		return $this->select('*')->where('game_id', $gameId)->run();
	}

	public function addDeck($data) {
		$dataList = $this->filterData($data);
		return $this->insert($dataList)->run();
	}

	public function removeDeck($data) {
		return $this->delete('id', $data->id)->run();
	}

	private function filterData($data) {
		$list = array('id', 'name', 'game_id');
		$dataList = $data->toArray();
		foreach ($dataList as $key => $value) {
			if (!in_array($key, $list))
				unset($dataList[$key]);
		}
		return $dataList;
	}
}