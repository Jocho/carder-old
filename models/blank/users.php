<?php
namespace Blank;
class Users extends Database {
	public $id;
	public $email;
	public $password;
	public $nick;
	public $created;
	public $tstamp;
	public $access_token;

	protected $table = 'users';
	private $spice   = 'PackMyBoxWithFiveDozenLiquorJugs';

	public function makeHash($value) {
		return md5($value . $this->spice . strlen($value));
	}

	public function checkAccessToken($user) {
		if ($user->has('id') && $user->has('access_token')) {
			$result = $this->select('id')->where(array(
				'id'           => $user->id, 
				'access_token' => $user->access_token
			))->run('object');
			return $result->has('id');
		}
		return FALSE;
	}

	public function addUser($email, $password) {
		if ($this->userExists($email))
			return FALSE;
		$insert = array(
			'email'    => $email,
			'password' => $this->makeHash($password), 
			'created'  => time(),
			'tstamp'   => time()
		);
		return $this->insert($insert)->run();
	}

	public function userExists($email) {
		$r = $this->select('id')->where('email', trim($email))->run();
		return count($r) ? TRUE : FALSE;
	}

	public function getUser($email, $password) {
		$pwd = $this->makeHash($password);

		$user = $this->select('*')->where(array('email' => $email, 'password' => $pwd))->showQuery()->run('object');
		if ($user->has('id')) {
			$hash = $this->makeHash($user->id . time());
			$this->update(array('access_token' => $hash))->where('id', $user->id)->run();
			$user->access_token = $hash;
			$user->del('password');

		}
		return $user;
	}

	public function getById($id) {
		$user = $this->select('*')->where('id', $id)->run('object');
		$user->del('password');
		return $user;
	}

	public function getByAccessToken($at) {
		$user = $this->select('*')->where('access_token', $at)->run('object');
		$user->del('password');
		return $user;
	}

	public function editUser($id, $data) {
		$update = array();

		if (isset($data->nickname))
			$update['nick'] = $data->nickname;
		if (isset($data->password))
			$update['password'] = $this->makeHash($data->password);

		if (count($update)) {
			return $this->update($update)->where('id', $id)->run();
		}
		return NULL;
	}
}
?>