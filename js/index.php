<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="robots" content="all" />
		<meta name="generator" content="sublime text 3" />
		<meta name="author" content="Marek J. Kolcun" />
		<meta name="keywords" content="lightweight, JS, framework, later.min.js" />
		<meta name="description" content="Later.js is lightweight JS framework!" />
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<link rel="stylesheet" type="text/css" href="css/blank.css" />
		<link rel="stylesheet" type="text/css" href="css/crdr.css" />
		<link href='http://fonts.googleapis.com/css?family=Roboto:300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="js/later.js"></script>
		<script type="text/javascript" src="js/crdr.js" defer="defer"></script>
		<!-- <link rel="shortcut icon" href="favicon.ico" /> -->
		
		<title>Later.js</title>
	</head>
	<body>
		<div data-ljs-s="table" class="table-main">
			<div class="column12">
				<div class="bar">
					<div class="left">
						<!-- <a href="#" class="bar__link">CrDr</a> -->
						<input type="text" name="title" class="bar__input" placeholder="Game title" />
						<!-- <input type="text" name="author" class="bar__input" placeholder="Author" /> -->
						<input type="text" name="version" class="bar__input size2" placeholder="Version" />
					</div>
					<div class="left main-actions">
						<a href="#deck" class="button bar__link">Edit deck</a>
						<a href="#export-import" data-ljs-l="exportImport" class="button bar__link">&uarr;/&darr; JSON</a>
						<a href="#about" class="button bar__link">About</a>
					</div>
					<div class="left select-actions hidden">
						<button class="button bar__button select-actions__all-cards" data-ljs-l="selectAllCards" title="All cards"><img src="images/ci-card.png" /></button>
						<button class="button bar__button select-actions__all-tokens" data-ljs-l="selectAllTokens" title="All tokens"><img src="images/ci-token.png" /></button>
						<button class="button bar__button select-actions__all-items" data-ljs-l="selectAllItems" title="All items"><img src="images/ci-all.png" /></button>
					</div>
					<div class="right actions hidden">
						<button class="button bar__button" data-ljs-l="flipAll" title="Flip"><img src="images/ci-flip.png" /></button>
						<button class="button bar__button" data-ljs-l="fanAll" title="Fan"><img src="images/ci-fan.png" /></button>
						<button class="button bar__button" data-ljs-l="lineAll" title="Line"><img src="images/ci-line.png" /></button>
						<button class="button bar__button" data-ljs-l="gridAll" title="Grid"><img src="images/ci-grid.png" /></button>
						<button class="button bar__button" data-ljs-l="pileAll" title="Pile"><img src="images/ci-pile.png" /></button>
						<button class="button bar__button" data-ljs-l="shuffleAll" title="Shuffle"><img src="images/ci-shuffle.png" /></button>
					</div>
					<div class="clear"></div>
				</div>
				<div class="table">{{card in cardList}}{{token in tokenList}}</div>
			</div>

			<div id="deck" class="modal">
				<div class="modal__modal-window">
					<div class="modal-window__header">
						<h2>Deck editor</h2>
					</div>
					<div class="modal-window__body">
						<ul>{{deckCard in cards}}</ul>
						<ul>{{deckToken in tokens}}</ul>
						<hr>
						<span>Total cards: {{cardAmount}}</span>
						<span>Total tokens: {{tokenAmount}}</span>
					</div>
					<div class="modal-window__footer taj">
						<button class="button" data-ljs-l="addCard">Add card</button>
						<button class="button" data-ljs-l="addToken">Add token</button>
						<span class="right">
							<a href="#" class="button" data-ljs-l="initDeck">Close</a>
						</span>
					</div>
				</div>
			</div>

			<div id="export-import" class="modal">
				<div class="modal__modal-window">
					<div class="modal-window__header">
						<h2>Import/Export as JSON string</h2>
					</div>
					<div class="modal-window__body">
						<textarea name="source" class="size12"></textarea>
					</div>
					<div class="modal-window__footer tar">
						<a href="#" class="button" data-ljs-l="initSource">Close</a>
					</div>
				</div>
			</div>

			<div id="about" class="modal">
				<div class="modal__modal-window">
					<div class="modal-window__header">
						<h2>CrDr v. 0.0.1</h2>
					</div>
					<div class="modal-window__body">
						<div class="row">
							<div class="column6">
								<p>Hello in CrDr - a card game tabletop simulator!</p>
								<p>Here you can create and test any card-based games that require nothing more that some tokens!</p>
								<p>This simulator was made to make life of a game designers a little bit easier. At least you don't have to cut and print all the cards just to realise that your concept of a card game does not work.</p>
								<p>The simulator is in an early development. There is a lot of work to do, but some crucial and most used operations are already available.</p>
							</div>
							<div class="column6">
								<p>Current list of possibilities:</p>
								<ul>
									<li>Create and edit a card (name, text of a card, amount)</li>
									<li>Create and edit a token (name, top text, bottom text of a token, amount)</li>
									<li>Select all the cards/tokens</li>
									<li>Flip selected items</li>
									<li>Arrange seleted items into a fan</li>
									<li>Arrange seleted items into a line</li>
									<li>Arrange seleted items into a grid</li>
									<li>Arrange seleted items into a grid</li>
									<li>Arrange seleted items into a pile</li>
									<li>Shuffle selected items (cards and tokens separately)</li>
									<li>Export card and token settings into JSON string</li>
									<li>Import card and token settings from JSON string</li>
								</ul>
								
							</div>
						</div>
					</div>
					<div class="modal-window__footer tar">
						<a href="#" class="button c--red">Close</a>
					</div>
				</div>
			</div>
		</div>

		<footer class="tac">
			created by <a href="http:jocho.sk" target="_blank">Jocho</a> &copy; 2015
		</footer>

		<template name="card">
			<div class="card" draggable="true">
				<div class="card__front">
					<div class="card__name">{{name}}</div>
					<div class="card__text">{{text}}</div>
				</div>
				<div class="card__back"></div>
			</div>
		</template>

		<template name="token">
			<div class="token" draggable="true">
				<div class="token__front">{{topval}}</div>
				<div class="token__back">{{botval}}</div>
			</div>
		</template>

		<template name="deckCard">
			<li class="deck__deck-item">
				<div class="deck-item__inline">{{amount}}&times; {{name}}</div>
				<div class="deck-item__editor">
					<input type="text" name="name" class="size8" />
					<button class="button c--red" data-ljs-l="removeCard">&times;</button>
					<textarea name="text" placeholder="Text on a card" class="size11"></textarea>
					<input type="number" min="1" step="1" name="amount" class="size2" />
				</div>
			</li>
		</template>

		<template name="deckToken">
			<li class="deck__deck-item">
				<div class="deck-item__inline">{{amount}}&times; {{name}}</div>
				<div class="deck-item__editor">
					<input type="text" name="name" placeholder="token name" class="size8" />
					<button class="button c--red" data-ljs-l="removeToken">&times;</button>
					<input type="text" name="topval" class="size11" placeholder="top value" />
					<input type="text" name="botval" class="size11" placeholder="bottom value" />
					<input type="number" min="1" step="1" name="amount" class="size2" />
				</div>
			</li>
		</template>
	</body>
</html>