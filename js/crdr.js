var app = new Later();
var table = app.section('table');

var Cookie = (function() {
	var _c = {};

	_c.set = function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	};

	_c.get = function(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ')
				c = c.substring(1);
			if (c.indexOf(name) == 0)
				return c.substring(name.length, c.length);
		}
		return '';
	};

	_c.is = function(cname) {
		var cookie = _c.get(cname);
		return cookie.length;
	};

	return _c;
});



String.prototype.hashCode = function() {
	var hash = 0, i, chr, len;
	if (this.length === 0)
		return hash;
	for (i = 0, len = this.length; i < len; i++) {
		chr   = this.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
		hash |= 0; // Convert to 32bit integer
	}
	return hash;
};

String.prototype.hashMe = function() {
	var hash = '';
	if (!this.length)
		return hash;

	var newThis = this.replace(/\s+/g, '-');
	return newThis;
}


// Sign in/up process
table.ctrl.set('signProcess', {
	type: 'signin',
	placeholder: 'E-mail',
	inputType: 'email',
	button: 'Next',
	step: 0,
	maxSteps: 1,
	data: '',
	userData: {},
	error: ''
});

table.call.register('signProcess.type', '_', function() {
	var mSteps = (table.ctrl.get('signProcess.type') == 'signin') ? 1 : 2;
	table.ctrl.set('signProcess', {
		placeholder: 'E-mail',
		inputType: 'email',
		button: 'Next',
		step: 0,
		maxSteps: mSteps,
		data: '',
		error: ''
		// userData: {}
	});
	table.call.run('signProcessButtonVal', '_');
});

table.call.register('signProcessButtonVal', '_', function() {
	var sp = table.ctrl.get('signProcess');
	var nextStep = parseInt(sp.step) + 1;
	var inOrUp = (sp.type == 'signin') ? 'Sign In' : 'Sign Up';
	var buttonVal = (sp.step == sp.maxSteps) ? inOrUp : 'Next';
	
	table.ctrl.set('signProcess.button', buttonVal);
});

table.call.register('signProcessNext', '_', function() {
	var sp = table.ctrl.get('signProcess');
	var data = sp.data;
	var step = sp.step;
	var nextStep = parseInt(step) + 1;
	var input = document.querySelector('.sign-process-input');
	
	table.ctrl.set('signProcess.data', '');
	table.ctrl.set('signProcess.step', nextStep);
	table.call.run('signProcessButtonVal', '_');


	switch (step) {
		case 0:
			table.ctrl.set('signProcess.userData.email', data);
			table.ctrl.set('signProcess.inputType', 'password');
			table.ctrl.set('signProcess.placeholder', 'Password');
			input.focus();
			break;
		case 1:
			table.ctrl.set('signProcess.userData.pass', data);
			table.ctrl.set('signProcess.inputType', 'password');
			table.ctrl.set('signProcess.placeholder', 'Password again');
			input.focus();
			break;
		case 2:
			table.ctrl.set('signProcess.userData.pass2', data);
			break;

		default:
			table.call.run('signProcess.type', '_');
			input.focus();
			break;
	}
	
	if (nextStep > sp.maxSteps) {
		var userData = table.ctrl.get('signProcess.userData');
		table.call.run('signProcess.type', '_');

		var toSend = {
			email: userData.email,
			password: userData.pass
		};

		toSend[table.ctrl.get('signProcess.type')] = true;

		if (typeof userData.pass2 !== 'undefined')
			toSend.password2 = userData.pass2;

		app.ajaxer.post('/sign', toSend, function(res) {
			var result = JSON.parse(res);
			if (typeof result.user !== 'undefined') {
				table.ctrl.set('user', result.user);
				table.ctrl.set('games', result.gameList);
				document.querySelector('#welcome .close').click();
			}
			else {
				table.ctrl.set('signProcess.error', 'Username or password was incorrect.');
			}
		});
	}
});

// login stuff
table.call.register('user', '_', function() {
	var user = table.ctrl.get('user');
	var ua = document.querySelector('.user-actions');
	var ga = document.querySelector('.guest-actions');
	if (JSON.stringify(user).length) {
		ua.classList.remove('hidden');
		ga.classList.add('hidden');
	}
	else {
		ua.classList.add('hidden');
		ga.classList.remove('hidden');
	}
});

// game actions
table.call.register('+gameItem+', '_', function(x) {
	var xd = x.split('.');
	var gameId = xd[1];
	var action = xd[3];
	var game = table.ctrl.get('games.' + gameId);

	switch (action) {
		case 'select':
			table.ctrl.set('game', game.gamedata);
			document.querySelector('#gamelist .close').click();
			table.call.run('initDeck', '_');
			break;

		case 'save':
			break;
		case 'delete':
			app.ajaxer.delete('/games/' + game.id, function(res) {
				var result = JSON.parse(res);
				var games = table.ctrl.get('games');
				games.splice(gameId, 1);
				table.ctrl.set('games', games);
				var currentGameId = table.ctrl.get('game.id');

				if (game.id == currentGameId)
					table.ctrl.set('game.id', 0);
			});
			break;
		default:
			break;
	}
});

table.call.register('game.title', '_', function() {
	var title = table.ctrl.get('game.title');
	var saveButton = document.querySelector('.save-game');
	if (title.length)
		saveButton.classList.remove('hidden');
	else
		saveButton.classList.add('hidden');

});

table.call.register('saveGame', '_', function() {
	var game = table.ctrl.get('game');
	var games = table.ctrl.get('games');
	var found = false;

	table.call.run('exportImport', '_');

	var data = {
		id: game.id,
		title: game.title,
		version: game.version,
		public: game.public,
		gamedata: table.ctrl.get('source')
	};

	for (var i = 0; i < games.length; i++) {
		if (game.id == games[i].id) {
			games[i] = game;
			app.ajaxer.put('/games/' + game.id, data);
			found = true;
			break;
		}
	}

	if (found) {
		table.ctrl.set('games', games);
	}
	else {
		
		// games.push(game);
		app.ajaxer.post('/games', data, function(res) {
			var result = JSON.parse(res);
			game.id = result.game.id;
			games.push(game);
			table.ctrl.set('games', games);
			table.ctrl.set('game.id', game.id);
		});
	}
});

// JSON export
table.call.register('exportImport', '_', function() {
	var game = table.ctrl.get('game');
	var list = ['cards', 'tokens'];

	game.cardList = (typeof game.cardList !== 'undefined') ? game.cardList : [];
	game.tokenList = (typeof game.tokenList !== 'undefined') ? game.tokenList : [];

	for (var x = 0; x < list.length; x++) {
		var items = [];
		var itemsInList = [];
		var itemList = (!x) ? game.cardList : game.tokenList;
		var ii = itemList.length;

		for (var i = 0; i < ii; i++) {
			var hashSource = JSON.parse(JSON.stringify(itemList[i]));
			delete(hashSource.flipped);
			delete(hashSource.x);
			delete(hashSource.y);
			var itemId = JSON.stringify(hashSource).hashCode();
			
			if (itemsInList.indexOf(itemId) == -1) {
				itemsInList.push(itemId);
				var item = JSON.parse(JSON.stringify(itemList[i]));
				item.positions = [[item.x, item.y]];
				item.flippedList = [item.flipped];
				item.amount = 1;
				delete(item.x);
				delete(item.y);
				delete(item.flipped);
				items.push(item);
			}
			else {
				var index = itemsInList.indexOf(itemId);
				items[index].positions.push([itemList[i].x, itemList[i].y]);
				items[index].flippedList.push(itemList[i].flipped);
				items[index].amount++;
			}
		}
		game[list[x]] = items;
	}

	// delete(game.source);
	delete(game.cardList);
	delete(game.tokenList);
	delete(game.cardAmount);
	delete(game.tokenAmount);
	table.ctrl.set('source', JSON.stringify(game));
});

table.call.register('initSource', '_', function() {
	var source = table.ctrl.get('source');
	if (JSON.parse(source)) {
		table.ctrl.set('game', JSON.parse(source), false);
		table.call.run('initDeck', '_');
	}
});

table.call.register('+amount', 'refresh', function() {
	var cardSum = 0;
	var tokenSum = 0;
	var cards = table.ctrl.get('game.cards');
	var tokens = table.ctrl.get('game.tokens');

	for (var i = 0; i < cards.length; i++) {
		cardSum += parseInt(cards[i].amount, 10);
	}

	for (var i = 0; i < tokens.length; i++) {
		tokenSum += parseInt(tokens[i].amount, 10);
	}

	table.ctrl.set('game.cardAmount', cardSum);
	table.ctrl.set('game.tokenAmount', tokenSum);

});

table.call.register('+amount', '_', function(x) {
	var xd = x.split('.');
	var type = xd[0];
	xd.pop();
	a = xd.join('.');
	var data = table.ctrl.get(a);
	
	data.positions = data.positions.reduce(function (x, y, z) {
		if (x.length > data.amount)
			x.pop();
		return x;
	}, data.positions);

	data.flippedList = data.flippedList.reduce(function (x, y, z) {
		if (x.length > data.amount)
			x.pop();
		return x;
	}, data.flippedList);

	while (data.positions.length < data.amount) {
		data.positions.push([0, 0]);
		data.flippedList.push(0);
	}

	table.ctrl.set(a + '.positions', data.positions);
	table.ctrl.set(a + '.flippedList', data.flippedList);
});

// card editing
table.call.register('addCard', '_', function() {
	var cards = table.ctrl.get('game.cards');
	cards.push(main.defCard);
	table.ctrl.set('game.cards', cards);
	table.call.run('+amount', 'refresh');
});

table.call.register('+removeCard', '_', function(x) {
	var xd = x.split('.');
	var cards = table.ctrl.get('game.cards');

	cards.splice(xd[2], 1);
	table.ctrl.set('game.cards', cards);
	table.call.run('+amount', 'refresh');
});

// token editing
table.call.register('addToken', '_', function() {
	var tokens = table.ctrl.get('game.tokens');
	tokens.push(main.defToken);
	table.ctrl.set('game.tokens', tokens);
	table.call.run('+amount', 'refresh');
});

table.call.register('+removeToken', '_', function(x) {
	var xd = x.split('.');
	var tokens = table.ctrl.get('game.tokens');

	tokens.splice(xd[1], 1);
	table.ctrl.set('game.tokens', tokens);
	table.call.run('+amount', 'refresh');
});


// selection actions
table.call.register('selectAllCards', '_', function(select) {
	select = typeof select === 'boolean' ? select : true;
	
	if (select)
		table.call.run('selectAllTokens', '_', false);

	var cards = document.querySelectorAll('.card');
	var ii = cards.length;
	for (var i = 0; i < ii; i++) {
		if (select)
			cards[i].classList.add('item__selected');
		else
			cards[i].classList.remove('item__selected');
	}

	main.toggleActions(true);
});

table.call.register('selectAllTokens', '_', function(select) {
	select = typeof select === 'boolean' ? select : true;

	if (select)
		table.call.run('selectAllCards', '_', false);

	var tokens = document.querySelectorAll('.token');
	var ii = tokens.length;
	for (var i = 0; i < ii; i++) {
		if (select)
			tokens[i].classList.add('item__selected');
		else
			tokens[i].classList.remove('item__selected');
	}

	main.toggleActions(true);
});

table.call.register('selectAllItems', '_', function() {
	var tokens = document.querySelectorAll('.token, .card');
	var ii = tokens.length;
	for (var i = 0; i < ii; i++) {
		tokens[i].classList.add('item__selected');
	}

	main.toggleActions(true);
});

// selected operations
table.call.register('flipAll', '_', function() {
	var selected = document.querySelectorAll('.item__selected');
	var ii = selected.length;
	for (var i = 0; i < ii; i++) {
		main.flip(selected[i]);
	}
});

table.call.register('pileAll', '_', function() {
	var selected = document.querySelectorAll('.item__selected');
	var ii = selected.length;
	
	var firstCardStyle = window.getComputedStyle(selected[0]);
	var top = parseInt(firstCardStyle.top, 10);
	var left = parseInt(firstCardStyle.left, 10);

	var plb = (ii > 16) ? 2 : 4;
	var ptb = (ii > 16) ? 2 : 4;
	var pt = 0;
	var pl = 0;

	for (var i = 0; i < ii; i++) {
		main.writePosition(selected[i], left + pl, top + pt);
		selected[i].style.top = top + pt + 'px';
		selected[i].style.left = left + pl + 'px';
		selected[i].style.transform = 'rotate(0deg)';

		pt += ptb;
		pl += plb;
	}
});

table.call.register('shuffleAll', '_', function() {
	var cards = table.ctrl.get('game.cardList');
	var tokens = table.ctrl.get('game.tokenList');

	var list = ['.card', '.token'];

	for (var b = 0; b < list.length; b++) {
		var selected = document.querySelectorAll(list[b] + '.item__selected');
		var usedList = (!b) ? cards : tokens;
		var ii = selected.length;

		var aa = Math.floor(Math.random() * ii);
		for (var a = 0; a < aa; a++) {
			for (var i = 1; i < ii; i++) {
				var newIndex = Math.floor(Math.random() * ii);
				var x = selected[newIndex].getAttribute('data-ljs-i');
				var xd = x.split('.');

				var y = selected[i].getAttribute('data-ljs-i');
				var yd = y.split('.');


				var tempItem = usedList[xd[2]];
				usedList[xd[2]] = usedList[yd[2]];
				usedList[yd[2]] = tempItem;
			}
		}

		if (!b)
			cards = usedList;
		else
			tokens = usedList;
	}

	table.ctrl.set('game.cardList', cards);
	table.ctrl.set('game.tokenList', tokens);
});

table.call.register('lineAll', '_', function(doFan) {
	doFan = (typeof doFan === 'boolean') ? doFan : false;
	var cards = table.ctrl.get('game.cards');
	var selected = document.querySelectorAll('.item__selected');
	var ii = selected.length;

	if (!ii)
		return;

	var firstCardStyle = window.getComputedStyle(selected[0]);
	var top = parseInt(firstCardStyle.top, 10);
	var left = parseInt(firstCardStyle.left, 10);

	var percentage = (ii < 16) ? 0.3 : 0.15;
	var lpBase = parseInt(parseInt(firstCardStyle.width, 10) * percentage);
	var lp = 0;

	if (doFan) {
		var baseDeg = (cards.length > 36) ? 360 / cards.length : 10;
		var deg = 0 - (baseDeg * Math.floor(ii / 2));
	}
	
	for (var i = 0; i < ii; i++) {
		main.writePosition(selected[i], left + lp, top);
		selected[i].style.top = top + 'px';
		selected[i].style.left = left + lp + 'px';

		if (doFan) {
			selected[i].style['transform-origin'] = '50% 100%';
			selected[i].style.transform = 'rotate(' + deg + 'deg)';
			deg += baseDeg;
		}
		else {
			lp += lpBase;
			selected[i].style.transform = 'rotate(0deg)';
		}
	}
});


table.call.register('fanAll', '_', function() {
	table.call.run('lineAll', '_', true);
});

table.call.register('gridAll', '_', function() {
	var cards = table.ctrl.get('game.cards');
	var selected = document.querySelectorAll('.item__selected');
	var ii = selected.length;

	if (!ii)
		return;

	var firstCardStyle = window.getComputedStyle(selected[0]);
	var top = parseInt(firstCardStyle.top, 10);
	var left = parseInt(firstCardStyle.left, 10);

	var ptb = parseInt(firstCardStyle.height, 10) * 1.2;
	var plb = parseInt(firstCardStyle.width, 10) * 1.2;
	var pt = 0;
	var pl = 0;

	var sqrt = Math.round(Math.sqrt(ii));

	for (var i = 0; i < ii; i++) {
		main.writePosition(selected[i], left + pl, top + pt);
		selected[i].style.top = top + pt + 'px';
		selected[i].style.left = left + pl + 'px';
		selected[i].style.transform = 'rotate(0deg)';

		if ((i + 1) % sqrt == 0) {
			pl = 0;
			pt += ptb;			
		}
		else {
			pl += plb;
		}
	}
});

table.call.register('initDeck', '_', function() {
	var list = ['game.cards', 'game.tokens'];
	var listLengths = [0, 0];

	for (var x = 0; x < list.length; x++) {
		var listList = ['game.cardList', 'game.tokenList'];
		var items = table.ctrl.get(list[x]);
		var itemList = [];
		var ii = items.length;
		listLengths[x] = ii;
		// table.ctrl.set(listList[x]);

		for (var i = 0; i < ii; i++) {
			for (j = 0; j < items[i].amount; j++) {
				var item = JSON.parse(JSON.stringify(items[i]));
				var pos = item.positions[j];
				item.flipped = item.flippedList[j];
				item.x = pos[0];
				item.y = pos[1];
				delete(item.positions);
				delete(item.flippedList);
				delete(item.amount);
				itemList.push(item);
			}
		}

		table.ctrl.set(listList[x], itemList);
	}

	table.call.run('game.title', '_');

	document.querySelector('.select-actions').classList.add('hidden');
	document.querySelector('.select-actions__all-cards').classList.add('hidden');
	document.querySelector('.select-actions__all-tokens').classList.add('hidden');
	document.querySelector('.select-actions__all-items').classList.add('hidden');
	
	if (listLengths[0] || listLengths[1])
		document.querySelector('.select-actions').classList.remove('hidden');

	if (listLengths[0] && listLengths[1])
		document.querySelector('.select-actions__all-items').classList.remove('hidden');
	
	if (listLengths[0])
		document.querySelector('.select-actions__all-cards').classList.remove('hidden');

	if (listLengths[1])
		document.querySelector('.select-actions__all-tokens').classList.remove('hidden');

	var items = document.querySelectorAll('.card, .token');
	var ii = items.length;

	for (var i = 0; i < ii; i++) {
		if (typeof items[i].dataset.listened === 'undefined') {
			var x = items[i].dataset['ljsI'];
			var itemData = table.ctrl.get(x);

			items[i].style.top = itemData.y + 'px';
			items[i].style.left = itemData.x + 'px';

			if (itemData.flipped)
				items[i].classList.add('flipped');
			
			items[i].addEventListener('dragstart', main.dragStart, false);
			items[i].addEventListener('click', main.flipEvent, false);
			items[i].dataset.listened = true;
		}
	}

	if (typeof tableElem.dataset.listened === 'undefined') {
		tableElem.addEventListener('dragover', main.dragOver, false);
		tableElem.addEventListener('drop', main.drop, false);
		tableElem.dataset.listened = true;
	}


});

var main = (function() {
	var _m = {};

	_m.mouseStart = [];
	_m.mouseEnd = [];

	_m.defGame = {
		id: 0,
		title: '',
		version: '',
		public: 0,
		cards: [],
		tokens: []
	};

	_m.defCard = {
		name: 'card',
		text: '',
		front: '',
		back: '',
		positions: [[0, 0]],
		flippedList: [0],
		amount: 1
	};

	_m.defToken = {
		name: 'token',
		topval: '',
		botval: '',
		positions: [[0, 0]],
		flippedList: [0],
		amount: 1
	}

	var cookie = new Cookie();

	_m.init = function() {
		table.ctrl.set('game', this.defGame);
		loginUser();
	};

	var loginUser = function() {
		if (!cookie.is('_bat'))
			return;

		var ck = cookie.get('_bat');
		app.ajaxer.post('/', {
			access_token: ck,
			signByAccessToken: true
		}, function(res) {
			var result = JSON.parse(res);
			if (typeof result.user !== 'undefined') {
				table.ctrl.set('user', result.user);
				table.ctrl.set('games', result.gameList);
			}
		});
	};

	_m.writePosition = function(elem, x, y) {
		var z = elem.dataset['ljsI'];

		if (typeof x !== 'undefined' && typeof y !== 'undefined') {
			table.ctrl.set(z + '.x', x, false);
			table.ctrl.set(z + '.y', y, false);
		}

		var flipped = elem.classList.contains('flipped');
		table.ctrl.set(z + '.flipped', flipped, false);
	};

	var offset_data; //Global variable as Chrome doesn't allow access to event.dataTransfer in dragover
	var draggedElem = '';

	_m.dragStart = function(event) {
		draggedElem = event.srcElement.getAttribute('data-ljs-i');
		event.srcElement.style.transform = 'rotate(0deg)';

		var style = window.getComputedStyle(event.target, null);
		offset_data = (parseInt(style.getPropertyValue("left"),10) - event.clientX) + ',' + (parseInt(style.getPropertyValue("top"),10) - event.clientY);
		event.dataTransfer.setData("text/plain",offset_data);

		// var ofX = event.clientX - event.offsetX;
		// var ofY = event.clientY - event.offsetY;
		var offset = getOffset(event);
		main.mouseStart = [event.clientX + parseInt(offset[0], 10), event.clientY + parseInt(offset[1], 10)];
	};

	_m.dragOver = function(event) {
		var offset = getOffset(event);
		var dm = document.querySelector('[data-ljs-i="' + draggedElem + '"]');
		dm.style.left = (event.clientX + parseInt(offset[0],10)) + 'px';
		dm.style.top = (event.clientY + parseInt(offset[1],10)) + 'px';
		event.preventDefault(); 
		return false; 
	};

	var getOffset = function(event) {
		try {
			offset = event.dataTransfer.getData("text/plain").split(',');
		} 
		catch(e) {
			offset = offset_data.split(',');
		}
		return offset;
	};

	_m.drop = function(event) { 
		var offset = getOffset(event);
		var x = event.clientX + parseInt(offset[0],10);
		var y = event.clientY + parseInt(offset[1],10);


		var distX = x - _m.mouseStart[0];
		var distY = y - _m.mouseStart[1];
		
		var selected = tableElem.querySelectorAll('.item__selected');
		var ii = selected.length;
		if (ii) {
			for (var i = 0; i < ii; i++) {
				var style = window.getComputedStyle(selected[i]);
				var top = parseInt(style.top, 10);
				var left = parseInt(style.left, 10);

				var newTop = top + distY;
				var newLeft = left + distX;

				selected[i].style.top = newTop + 'px';
				selected[i].style.left = newLeft + 'px';

				_m.writePosition(selected[i], newLeft, newTop);
			}
		}
		else {
			var dm = document.querySelector('[data-ljs-i="' + draggedElem + '"]');
			dm.style.left = (x) + 'px';
			dm.style.top = (y) + 'px';
			
			_m.writePosition(dm, x, y);

			setTop(dm);
		}

		event.preventDefault();
		return false;
	};

	_m.flipEvent = function(event) {
		_m.flip(event.target.parentElement);
	};

	_m.flip = function(card) {
		card.classList.toggle('flipped');
		main.writePosition(card);
		setTop(card);
	};

	var setTop = function(elem) {
		var cards = document.querySelectorAll('.card, .token');
		var ii = cards.length;
		for (var i = 0; i < ii; i++) {
			cards[i].classList.remove('item__top');
		}
		elem.classList.add('item__top');
	};

	_m.selectCards = function() {
		var cards = tableElem.querySelectorAll('.card, .token');
		var ii = cards.length;

		var topMin, topMax, leftMin, leftMax = 0;

		if (main.mouseStart[1] > main.mouseEnd[1]) {
			topMin = main.mouseEnd[1];
			topMax = main.mouseStart[1];
		}
		else {
			topMin = main.mouseStart[1];
			topMax = main.mouseEnd[1];
		}

		if (main.mouseStart[0] > main.mouseEnd[0]) {
			leftMin = main.mouseEnd[0];
			leftMax = main.mouseStart[0];
		}
		else {
			leftMin = main.mouseStart[0];
			leftMax = main.mouseEnd[0];
		}

		var selectedCards = 0;

		for (var i = 0; i < ii; i++) {
			var style = window.getComputedStyle(cards[i]);
			var top = parseInt(style.top, 10);
			var left = parseInt(style.left, 10);
			var widtHalf = parseInt(style.width, 10) / 2;
			var heightHalf = parseInt(style.height, 10) / 2;

			if ((top + heightHalf >= topMin && top + heightHalf <= topMax) && (left + widtHalf >= leftMin && left + widtHalf <= leftMax)) {
				selectedCards++;
				cards[i].classList.add('item__selected');
			}
			else
				cards[i].classList.remove('item__selected');
		}

		if (selectedCards)
			_m.toggleActions(true);
		else
			_m.toggleActions(false);
	};

	_m.toggleActions = function(show) {
		show = (typeof show === 'boolean') ? show : false;
		var actionBarCl = document.querySelector('.actions').classList;
		if (show)
			actionBarCl.remove('hidden');
		else
			actionBarCl.add('hidden');
	};

	_m.itemExists = function(list, name, ignore) {
		var ii = list.length;
		ignore--;

		for (var i = 0; i < ii; i++) {
			if (list[i].name == name) {
				if (ignore)
					ignore--;
				else
					return true;
			}
		}
		return false;
	};

	return _m;
}());

var cookie = new Cookie();
var tableElem = document.querySelector('.table');
main.init();


tableElem.addEventListener('mousedown', function(e) {
	var ofX = e.clientX - e.offsetX;
	var ofY = e.clientY - e.offsetY;
	main.mouseOffset = [ofX, ofY];
	main.mouseStart = [e.clientX - ofX, e.clientY - ofY];

	if (e.target == tableElem) {
		
		var border = document.createElement('div');
		border.setAttribute('class', 'border');
		border.style.top = main.mouseStart[1] + 'px';
		border.style.left = main.mouseStart[0] + 'px';
		border.style.bottom = 'auto';
		border.style.right = 'auto';
		border.style.width = 0;
		border.style.height = 0;

		tableElem.appendChild(border);
	}
}, false);

tableElem.addEventListener('mousemove', function(e) {
	var border = tableElem.querySelector('.border');
	if (border) {
		var style = window.getComputedStyle(tableElem);
		var width = (e.clientX - main.mouseStart[0]) - main.mouseOffset[0];
		var height = (e.clientY - main.mouseStart[1]) - main.mouseOffset[1];
		var val = 2;

		if (width > 0) {
			border.style.left = main.mouseStart[0] + 'px';
			border.style.right = 'auto';
			width -= val;
		}
		else {
			border.style.left = 'auto';
			border.style.right = parseInt(style.width, 10) - main.mouseStart[0] + 'px';
			width += val;
		}

		if (height > 0) {
			border.style.top = main.mouseStart[1] + 'px';
			border.style.bottom = 'auto';
			height -= val;
		}
		else {
			border.style.top = 'auto';
			border.style.bottom = parseInt(style.height, 10) - main.mouseStart[1] + 'px';
			height += val;
		}

		border.style.width = Math.abs(width) + 'px';
		border.style.height = Math.abs(height) + 'px';
	}
}, false);

tableElem.addEventListener('mouseup', function(e) {
	main.mouseEnd = [e.clientX - main.mouseOffset[0], e.clientY - main.mouseOffset[1]];

	var border = tableElem.querySelector('.border');
	if (border) {
		var border = tableElem.querySelector('.border');
		tableElem.removeChild(border);
		main.selectCards();
	}
	else
		main.mouseEnd = [];
}, false);