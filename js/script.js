Card = {
	flip: function(me) {
		if ($(me).hasClass('flipped'))
			$(me).removeClass('flipped');
		else
			$(me).addClass('flipped');
	},

	
}

Deck = {
	addDeck: function(event) {
		event.preventDefault();
		var self = this;
		var data = {
			name: $('#deck-name').val(),
			game_id: window.location.pathname.split('/').pop(),
			addDeck: true
		}

		console.log(data);

		$.post(window.location.pathname, data, function(result) {
			var res = JSON.parse(result);
			self.listDecks(res);
			console.log(res);
		});
	},

	removeDeck: function (event, id) {
		event.preventDefault();
		var self = this;
		var data = {
			'id': id,
			game_id: window.location.pathname.split('/').pop(),
			removeDeck: true
		}

		$.post(window.location.pathname, data, function(result) {
			var res = JSON.parse(result);
			self.listDecks(res);
			console.log(res);
		});
	},

	selectDeck: function(event, me) {
		$('#deck-name').val($(me).text());
	},

	listDecks: function(source) {
		var data = {
			game_id: window.location.pathname.split('/').pop(),
			listDecks: true
		}
		$.post(window.location.pathname, data, function(result) {
			var res = JSON.parse(result);
			$('.deck-list').replaceWith(res);
		});	
	}
}