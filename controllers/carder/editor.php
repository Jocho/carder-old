<?php
namespace Carder;
class Editor extends SignedGear {
	protected $game;
	protected $decks;

	public function init() {
		parent::init();

		if ($this->blank->isAjaxRequest)
			$this->useTemplate('ajax_template');

		$this->gear('\Carder\Cogs\Decks', 'decks');

		$this->loadGameData();
	}
	public function render() {
		$this->template->game  = $this->game;
		$this->template->decks = $this->models->carderDecks->getByGameId($this->game->id);
	}

	/* FORMS */
	public function newGameForm($form) {
		$form->set('user_id', $this->user->id);
		$result = $this->models->carderGames->addGame($form);

		if ($result)
			$this->gears->info->say('Game ' . $form->get('title') . ' was created.', 'cGreen');
		else
			$this->gears->info->say('Game ' . $form->get('title') . ' could not be created.', 'cRed');
		$this->redirect('this', TRUE);
	}

	private function loadGameData() {
		if ($this->session->has('game')) {
			$this->game = $this->session->game;
		}
		elseif (isset($this->router->query['game_id']) && $this->router->query['game_id'] > 0) {
			$this->game = $this->models->carderGames->getByUserGameId($this->user->id, $this->router->query['game_id']);
			if (!$this->session->has('game')) {
				$this->session->set('game', $this->game);
				$this->gears->info->say('Game loaded.', 'cGreen');
			}

			$this->gears->decks->loadDecks($this->game->id);
		}
		else {
			$this->gears->info->say('Could not load game.', 'cRed');
		}
	}
}
?>