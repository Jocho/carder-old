<?php
namespace Carder\Cogs;
class Decks extends \Carder\SignedGear {
	private $decks;

	public function init() {
		$this->useTemplate('ajax_template');
	}

	public function render() {
		$this->template->decks = $this->decks;
	}

	public function loadDecks($gameId) {
		if ($this->session->has('decks') && is_array($this->session->get('decks')))
			$this->decks = $this->session->get('decks');
		else {
			$this->decks = $this->models->carderDecks->getByGameId($gameId);
			$this->session->set('decks', $this->decks);
		}
	}

	public function listDecksForm($form) {
		$this->loadDecks($form->get('game_id'));
		$this->render();
		echo json_encode($this->view());
		if ($this->blank->isAjaxRequest)
			die();
		$this->redirect('this');
	}

	public function addDeckForm($form) {
		$this->models->carderDecks->addDeck($form);
		$this->listDecksForm($form);
	}

	public function removeDeckForm($form) {
		$this->models->carderDecks->removeDeck($form);
		$this->listDecksForm($form);
	}
}
?>