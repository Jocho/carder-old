<?php
namespace Carder;
class Home extends \Blank\Gear {
	public function init() {
		$this->useTemplate('frontend_template');
		$this->gear('\Blank\Sign', 'sign');
		$this->gear('\Blank\Info', 'info');
		// $this->gear('\Blank\Footer');
	}
	public function render() {}

	public function logoutRender() {
		$this->gears->sign->logoutHandler();
	}
}
?>