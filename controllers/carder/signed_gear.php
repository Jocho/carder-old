<?php
namespace Carder;
class SignedGear extends \Blank\Gear {
	protected $user;

	public function init() {
		$this->useTemplate('backend_template');
		$this->gear('\Blank\Sign', 'sign');
		$this->gear('\Blank\Info', 'info');
		
		if (!$this->gears->sign->isLogged())
			$this->redirect('/');

		if ($this->gears->sign->userData->hasAny())
			$this->user = $this->gears->sign->userData;

		if ($this->blank->isAjaxRequest)
			$this->template->user = $this->user;
	}
}
?>