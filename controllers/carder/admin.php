<?php
namespace Carder;
class Admin extends SignedGear {
	
	public function init() {
		parent::init();

		if ($this->session->has('game')) {
			$this->session->del('game');
		}

		if ($this->session->has('decks')) {
			$this->session->del('decks');
		}
	}
	
	public function render() {
		$this->template->gameList = $this->listByUser();
	}

	public function listByUser() {
		$games = $this->models->carderGames->listByUserId($this->user->id);
		foreach ($games as $k => $game) {
			$games[$k]->gamedata = json_decode(stripcslashes($game->gamedata));
			$games[$k]->gamedata->id = $game->id;
		}
		// var_dump($games);
		return $games;
	}


	/* FORMS */
	public function newGameForm($form) {
		$form->set('user_id', $this->user->id);
		$result = $this->models->carderGames->addGame($form);

		if ($result)
			$this->gears->info->say('Game ' . $form->get('title') . ' was created.', 'cGreen');
		else
			$this->gears->info->say('Game ' . $form->get('title') . ' could not be created.', 'cRed');
		$this->redirect('this', TRUE);
	}
}
?>