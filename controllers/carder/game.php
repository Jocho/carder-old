<?php
namespace Carder;
class Game extends SignedGear {
	
	public function init() {
		parent::init();
		$this->useTemplate('frontend_template');

		// $res = $this->models->carderGames->getByUserGameId($this->user->id, $this->router->query['game_id']);
		// var_dump ($res);

		// if ($this->requestMethod == 'get' || $this->models->carderGames->getByUserGameId($this->user->id, $this->router->query['game_id']))
		$this->{$this->blank->requestMethod}($this->router);
	}

	private function get($router) {

	}

	private function post($router) {
		$this->post->del('id');
		$this->post->user_id = $this->user->id;

		$gameId = $this->models->carderGames->addGame($this->post);

		$this->post->id = $gameId;
		$this->template->game = $this->post;
	}

	private function put($router) {
		// var_dump($this->post);
		$result = $this->models->carderGames->saveGame($this->post);
		$this->template->gameSaved = $result;
	}

	private function delete($router) {
		if ($this->user->get('id') !== NULL && isset($this->router->query['game_id'])) {
			$game = $this->models->carderGames->getByUserGameId($this->user->id, $this->router->query['game_id']);

			if ($game->get('id') !== NULL)
				$this->models->carderGames->delGame($game->id);

			$this->template->gameList = $this->models->carderGames->listByUserId($this->user->id);
		}
	}

	public function render() {}
}