<?php
namespace Blank;
class Home extends Gear {
	public function init() {
		$this->useTemplate('lightweight_frontend_template');
		$this->gear('\Blank\Sign', 'sign');
		$this->gear('\Blank\Footer');
	}
	public function render() {
		$this->template->isLogged = $this->gears->sign->isLogged();
	}
}
?>