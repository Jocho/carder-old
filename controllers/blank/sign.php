<?php
namespace Blank;
class Sign extends Gear {
	public $userData;
	private $userModel;

	public function init() {
		$this->userData = new BlankObject;
		$this->gear('\Blank\Info', 'info');
		$this->userModel = $this->models->blankUsers;
		
		// var_dump($this->userModel->makeHash('admin'));
		// die();

		$cookie = new BlankObject;
		if ($this->cookie->has('_bat') && !$this->session->has('user')) {
			$user = $this->models->blankUsers->getByAccessToken($this->cookie->get('_bat'));

			if ($user->has('id'))
				$this->session->set('user', $user);
		}
		elseif ($this->session->has('user') && (!$this->cookie->has('_bat') || $this->cookie->get('_bat') != $this->session->user->get('access_token'))) {
			$this->cookie->set('_bat', $this->session->user->get('access_token'));
		}

		if ($this->session->has('user') && $this->userModel->checkAccessToken($this->session->user)) {
			$this->userData = $this->session->user;
		}
		// var_dump ($this->userData, $this->cookie);
	}

	public function signinForm($form) {
		$this->userData = $this->userModel->getUser($form->email, $form->password);

		if ($this->userData->get('id') !== NULL) {
			$this->session->set('user', $this->userData);
			$this->cookie->set('_bat', $this->userData->get('access_token'));
			$this->gears->info->say('Welcome, ' . $this->userData->email, 'b--green');
			$this->redirect('!accepted', TRUE);
		}
		else {
			$this->gears->info->say('Cannot login user. Check email and password.', 'b--red');
			$this->redirect('!refused');
		}
	}

	public function signupForm($form) {
		if ($this->userModel->userExists($form->email)) {
			$this->gears->info->say('User creation failed. User ' . $form->email . ' already exists.', 'b--red');
			$this->redirect('!userAlreadyExists');
		}

		if (!$this->checkPassword($form))
			$this->redirect('!incorrectPassword');

		$result = $this->userModel->addUser($form->email, $form->password);
		if ($result) {
			$this->gears->info->say('User ' . $form->email . ' was successfully created.', 'b--green');
			$this->session->set('user', $this->models->blankUsers->getById($result));
			$this->cookie->set('_bat', $this->session->user->get('access_token'));
		}
		else
			$this->gears->info->say('User with name ' . $form->email . ' could not be created.', 'b--red');
		$this->redirect('!userRegistered');
	}

	public function signByAccessTokenForm($form) {
		$user = $this->userModel->getByAccessToken($form->access_token);

		if ($user->get('id') !== NULL) {
			$this->userData = $user;
			$this->redirect('!accepted', TRUE);
		}
		else {
			$this->redirect('!refused');
		}
	}

	public function isLogged() {
		return $this->userData->hasAny();
	}

	public function refreshUser() {
		$this->userData = $this->userModel->getById($this->userData->id);
		$this->session->set('user', $this->userData);
	}

	private function checkPassword($form) {
		if (isset($form->password) && isset($form->password2) && strlen($form->password)) {

			if ($form->has('old_password') && !strlen($form->get('old_password'))) {
				$this->gears->info->say('Old password was not filled.', 'b--orange');
				return FALSE;
			}

			// var_dump ($form->get('old_password'));
			// var_dump ($this->userModel->makeHash($form->get('old_password')));
			// var_dump ($this->userData->password);
			// die();

			if ($form->has('old_password') && $this->userModel->makeHash($form->get('old_password')) != $this->userData->password) {
				$this->gears->info->say('Old password does not match', 'b--red');
				return FALSE;
			}

			if(strlen($form->password) < 5) {
				$this->gears->info->say('Password must be at least 5 characters long.', 'b--red');
				return FALSE;
			}

			if($form->password != $form->password2) {
				$this->gears->info->say('Passwords do not match.', 'b--red');
				return FALSE;
			}
			return TRUE;
		}
		return NULL;
	}

	public function editUser($data) {
		$chp = $this->checkPassword($data);
		if ($chp === FALSE)
			$this->redirect('this');
		elseif ($chp === NULL) {
			$data->del('old_password');
			$data->del('password');
			$data->del('password2');
		}
		
		$result = $this->userModel->editUser($this->userData->id, $data);
		if ($result === TRUE) {
			$this->refreshUser();
			$this->gears->info->say('User settings have been successfully edited.', 'b--green');
		}
		elseif ($result === FALSE)
			$this->gears->info->say('User settings could not be edited.', 'b--red');
		else
			$this->gears->info->say('User settings were not changed.', 'b--orange');
	}

	public function logoutHandler() {
		$this->session->del();
		$this->cookie->del('_bat');
		$this->gears->info->say('You have been successfully logged out.', 'b--green');
		$this->router->query = array();
		$this->redirect('!signedOut');
	}

	public function render() {}
}
?>