<?php
namespace Blank;
class Admin extends Gear {
	public function init() {
		$this->useTemplate('lightweight_frontend_template');
		$this->gear('\Blank\Sign', 'sign');
		$this->gear('\Blank\Footer');

		if (!$this->gears->sign->isLogged())
			$this->redirect('/');
	}
	public function render() {
		$this->template->user = $this->session->get('user');
	}
}
?>